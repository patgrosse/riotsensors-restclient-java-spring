# riotsensors-restclient-java-spring

A Java REST client for the riotsensors server using the Spring Framework.

## Build

This project uses Maven for building so simply use:

```
mvn install
```