package de.patgrosse.riotsensors.constants;

/**
 * All possible types of cache settings
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public enum LambdaCacheType {
    NO_CACHE(1),
    CALL_ONCE(2),
    CACHE_ONLY(3),
    CACHE_ON_TIMEOUT(4);

    private int code;

    LambdaCacheType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static LambdaCacheType fromCode(int code) {
        for (LambdaCacheType lambdaType : LambdaCacheType.values()) {
            if (lambdaType.getCode() == code) {
                return lambdaType;
            }
        }
        return null;
    }
}