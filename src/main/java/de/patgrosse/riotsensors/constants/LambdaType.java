package de.patgrosse.riotsensors.constants;

/**
 * All possible types of lambdas
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public enum LambdaType {
    INTEGER(1),
    DOUBLE(2),
    STRING(3);

    private int code;

    LambdaType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static LambdaType fromCode(int code) {
        for (LambdaType lambdaType : LambdaType.values()) {
            if (lambdaType.getCode() == code) {
                return lambdaType;
            }
        }
        return null;
    }
}
