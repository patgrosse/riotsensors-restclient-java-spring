package de.patgrosse.riotsensors.constants;

/**
 * All possible types of error codes
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public enum LambdaCallErrorType {
    NOT_FOUND(-1),
    CALL_WRONGTYPE(-2),
    CALL_TIMEOUT(-3),
    CALL_CACHE_EMPTY(-4),
    CALL_CACHE_TIMEOUT_EMPTY(-5);

    private int code;

    LambdaCallErrorType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static LambdaCallErrorType fromCode(int code) {
        for (LambdaCallErrorType lambdaType : LambdaCallErrorType.values()) {
            if (lambdaType.getCode() == code) {
                return lambdaType;
            }
        }
        return null;
    }
}
