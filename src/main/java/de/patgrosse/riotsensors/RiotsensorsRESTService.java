package de.patgrosse.riotsensors;

import de.patgrosse.riotsensors.constants.LambdaType;
import de.patgrosse.riotsensors.model.LambdaCacheList;
import de.patgrosse.riotsensors.model.LambdaCallResult;
import de.patgrosse.riotsensors.model.LambdaList;

/**
 * Provides methods to get information from a riotsensors REST server
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public interface RiotsensorsRESTService {
    /**
     * Request a list of all registered lambdas
     *
     * @return An entity representing the list
     */
    LambdaList listLambdas();

    /**
     * Request a list of all registered lambdas by a specific type
     *
     * @param type Lambda type to search for
     * @return An entity representing the list
     */
    LambdaList listLambdas(LambdaType type);

    /**
     * Request a list of all registered lambdas and their cached values
     *
     * @return An entity representing the list
     */
    LambdaCacheList listLambdasWithCache();

    /**
     * Request a list of all registered lambdas by a specific type and their cached values
     *
     * @param type Lambda type to search for
     * @return An entity representing the list
     */
    LambdaCacheList listLambdasWithCache(LambdaType type);

    /**
     * Call a lambda by it's id
     *
     * @param id   Id of the lambda
     * @param type Expected lambda type
     * @return An entity representing the result (on success and on failure)
     */
    LambdaCallResult callLambdaById(int id, LambdaType type);

    /**
     * Call a lambda by it's name
     *
     * @param name Name of the lambda
     * @param type Expected lambda type
     * @return An entity representing the result (on success and on failure)
     */
    LambdaCallResult callLambdaByName(String name, LambdaType type);
}
