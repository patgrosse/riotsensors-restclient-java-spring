package de.patgrosse.riotsensors.impl;

import de.patgrosse.riotsensors.RiotsensorsRESTService;
import de.patgrosse.riotsensors.constants.LambdaType;
import de.patgrosse.riotsensors.model.LambdaCacheList;
import de.patgrosse.riotsensors.model.LambdaCallResult;
import de.patgrosse.riotsensors.model.LambdaList;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

public class RiotsensorsRESTServiceImpl implements RiotsensorsRESTService {
    private RestTemplate restTemplate;
    private UriComponentsBuilder listURL;
    private UriComponentsBuilder listTypeURL;
    private UriComponentsBuilder showcacheURL;
    private UriComponentsBuilder showcacheTypeURL;
    private UriComponentsBuilder callByIdURL;
    private UriComponentsBuilder callByNameURL;

    RiotsensorsRESTServiceImpl(String host, int port, String version) {
        if (host == null) {
            throw new IllegalArgumentException("host is null");
        }
        if (port < 1) {
            throw new IllegalArgumentException("illegal port");
        }
        if (version == null) {
            throw new IllegalArgumentException("version is null");
        }
        restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return !(response.getStatusCode() == HttpStatus.OK || response.getStatusCode() == HttpStatus.NOT_FOUND);
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                throw new RestClientException("Received unacceptable return code " + response.getStatusCode() + " " + response.getStatusCode());
            }
        });
        UriComponentsBuilder base = UriComponentsBuilder.newInstance().scheme("http").host(host).port(port).pathSegment(version);
        listURL = ((UriComponentsBuilder) base.clone()).pathSegment("list");
        listTypeURL = ((UriComponentsBuilder) listURL.clone()).query("type={type}");
        showcacheURL = ((UriComponentsBuilder) base.clone()).pathSegment("showcache");
        showcacheTypeURL = ((UriComponentsBuilder) showcacheURL.clone()).query("type={type}");
        callByIdURL = ((UriComponentsBuilder) base.clone()).pathSegment("call", "id", "{type}", "{id}");
        callByNameURL = ((UriComponentsBuilder) base.clone()).pathSegment("call", "name", "{type}", "{name}");
    }

    @Override
    public LambdaList listLambdas() {
        return listLambdas(null);
    }

    @Override
    public LambdaList listLambdas(LambdaType type) {
        if (type == null) {
            return restTemplate.getForObject(listURL.toUriString(), LambdaList.class);
        } else {
            return restTemplate.getForObject(listTypeURL.buildAndExpand(type.getCode()).toUriString(), LambdaList.class);
        }
    }

    @Override
    public LambdaCacheList listLambdasWithCache() {
        return listLambdasWithCache(null);
    }

    @Override
    public LambdaCacheList listLambdasWithCache(LambdaType type) {
        if (type == null) {
            return restTemplate.getForObject(showcacheURL.toUriString(), LambdaCacheList.class);
        } else {
            return restTemplate.getForObject(showcacheTypeURL.buildAndExpand(type.getCode()).toUriString(), LambdaCacheList.class);
        }
    }

    @Override
    public LambdaCallResult callLambdaById(int id, LambdaType type) {
        if (type == null) {
            throw new IllegalArgumentException("type is null");
        }
        return restTemplate.getForObject(callByIdURL.buildAndExpand(type.getCode(), id).toUriString(), LambdaCallResult.class);
    }

    @Override
    public LambdaCallResult callLambdaByName(String name, LambdaType type) {
        if (type == null) {
            throw new IllegalArgumentException("type is null");
        }
        return restTemplate.getForObject(callByNameURL.buildAndExpand(type.getCode(), name).toUriString(), LambdaCallResult.class);
    }
}
