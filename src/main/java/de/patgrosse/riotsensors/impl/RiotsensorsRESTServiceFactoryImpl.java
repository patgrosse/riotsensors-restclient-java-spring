package de.patgrosse.riotsensors.impl;

import de.patgrosse.riotsensors.RiotsensorsRESTService;
import de.patgrosse.riotsensors.RiotsensorsRESTServiceFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Component
public class RiotsensorsRESTServiceFactoryImpl implements RiotsensorsRESTServiceFactory {
    @Override
    public RiotsensorsRESTService createRiotsensorsRESTService(String host, int port, String version) {
        return new RiotsensorsRESTServiceImpl(host, port, version);
    }
}
