package de.patgrosse.riotsensors.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import de.patgrosse.riotsensors.constants.LambdaCacheType;

import java.io.IOException;

/**
 * JSON deserializer for {@link LambdaCacheType}
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCacheTypeJacksonDeserializer extends JsonDeserializer<LambdaCacheType> {
    @Override
    public LambdaCacheType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LambdaCacheType.fromCode(p.getIntValue());
    }
}
