package de.patgrosse.riotsensors.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import de.patgrosse.riotsensors.constants.LambdaCallErrorType;

import java.io.IOException;

/**
 * JSON deserializer for {@link LambdaCallErrorType}
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCallErrorTypeJacksonDeserializer extends JsonDeserializer<LambdaCallErrorType> {
    @Override
    public LambdaCallErrorType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LambdaCallErrorType.fromCode(p.getIntValue());
    }
}
