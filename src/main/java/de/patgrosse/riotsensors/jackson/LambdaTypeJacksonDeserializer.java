package de.patgrosse.riotsensors.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import de.patgrosse.riotsensors.constants.LambdaType;

import java.io.IOException;

/**
 * JSON deserializer for {@link LambdaType}
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaTypeJacksonDeserializer extends JsonDeserializer<LambdaType> {
    @Override
    public LambdaType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LambdaType.fromCode(p.getIntValue());
    }
}
