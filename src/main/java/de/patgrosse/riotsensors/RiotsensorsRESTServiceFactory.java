package de.patgrosse.riotsensors;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Factory for {@link RiotsensorsRESTService} instances and Spring configuration for autowiring
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
@Configuration
@ComponentScan(basePackageClasses = RiotsensorsRESTServiceFactory.class)
public interface RiotsensorsRESTServiceFactory {
    /**
     * Get a new instance of a {@link RiotsensorsRESTService}
     *
     * @param host    Host to pass to the instance
     * @param port    Port to pass to the instance
     * @param version riotsensors REST version to pass to the instance
     * @return A new instance
     */
    RiotsensorsRESTService createRiotsensorsRESTService(String host, int port, String version);
}
