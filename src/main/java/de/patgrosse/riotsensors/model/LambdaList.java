package de.patgrosse.riotsensors.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Map;

/**
 * List of multiple lambdas
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaList {
    private Map<Integer, Lambda> lambdas;
    private int count;

    public Map<Integer, Lambda> getLambdas() {
        return lambdas;
    }

    public void setLambdas(Map<Integer, Lambda> lambdas) {
        this.lambdas = lambdas;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
