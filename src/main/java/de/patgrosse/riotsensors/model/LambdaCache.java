package de.patgrosse.riotsensors.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Cache content of a registered lambda
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCache {
    private boolean cache_available;
    private Object cached_result;

    public boolean isCache_available() {
        return cache_available;
    }

    public void setCache_available(boolean cache_available) {
        this.cache_available = cache_available;
    }

    public Object getCached_result() {
        return cached_result;
    }

    public void setCached_result(Object cached_result) {
        this.cached_result = cached_result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
