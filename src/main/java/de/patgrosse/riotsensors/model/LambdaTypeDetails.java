package de.patgrosse.riotsensors.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.patgrosse.riotsensors.constants.LambdaType;
import de.patgrosse.riotsensors.jackson.LambdaTypeJacksonDeserializer;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Information about the type of a lambda
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaTypeDetails {
    @JsonDeserialize(using = LambdaTypeJacksonDeserializer.class)
    @JsonProperty("code")
    private LambdaType type;
    private String string;

    public LambdaType getType() {
        return type;
    }

    public void setType(LambdaType type) {
        this.type = type;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
