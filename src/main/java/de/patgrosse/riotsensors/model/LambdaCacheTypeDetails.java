package de.patgrosse.riotsensors.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.patgrosse.riotsensors.constants.LambdaCacheType;
import de.patgrosse.riotsensors.jackson.LambdaCacheTypeJacksonDeserializer;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Information about the cache type of a lambda
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCacheTypeDetails {
    @JsonDeserialize(using = LambdaCacheTypeJacksonDeserializer.class)
    @JsonProperty("policy")
    private LambdaCacheType type;
    private String string;

    public LambdaCacheType getType() {
        return type;
    }

    public void setType(LambdaCacheType type) {
        this.type = type;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
