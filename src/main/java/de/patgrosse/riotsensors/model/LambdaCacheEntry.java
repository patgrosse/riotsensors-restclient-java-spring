package de.patgrosse.riotsensors.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Lambda information and the cache content
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCacheEntry {
    private Lambda lambda;
    private LambdaCache cache;

    public Lambda getLambda() {
        return lambda;
    }

    public void setLambda(Lambda lambda) {
        this.lambda = lambda;
    }

    public LambdaCache getCache() {
        return cache;
    }

    public void setCache(LambdaCache cache) {
        this.cache = cache;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
