package de.patgrosse.riotsensors.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A registered lambda and it's properties
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class Lambda {
    private int id;
    private String name;
    private LambdaTypeDetails type;
    private LambdaCacheTypeDetails cache;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LambdaTypeDetails getType() {
        return type;
    }

    public void setType(LambdaTypeDetails type) {
        this.type = type;
    }

    public LambdaCacheTypeDetails getCache() {
        return cache;
    }

    public void setCache(LambdaCacheTypeDetails cache) {
        this.cache = cache;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
