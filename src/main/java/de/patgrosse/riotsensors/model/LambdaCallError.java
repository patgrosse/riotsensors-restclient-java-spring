package de.patgrosse.riotsensors.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.patgrosse.riotsensors.constants.LambdaCallErrorType;
import de.patgrosse.riotsensors.jackson.LambdaCallErrorTypeJacksonDeserializer;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Details of an error that occurred on a lambda call
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCallError {
    @JsonDeserialize(using = LambdaCallErrorTypeJacksonDeserializer.class)
    @JsonProperty("code")
    private LambdaCallErrorType type;
    private String string;

    public LambdaCallErrorType getType() {
        return type;
    }

    public void setType(LambdaCallErrorType type) {
        this.type = type;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
