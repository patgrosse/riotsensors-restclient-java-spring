package de.patgrosse.riotsensors.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Result of a lambda call (on success and on failure)
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCallResult {
    private boolean success;
    private Lambda lambda;
    private LambdaCallCacheProperties cache;
    private Object result;
    private LambdaCallError error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Lambda getLambda() {
        return lambda;
    }

    public void setLambda(Lambda lambda) {
        this.lambda = lambda;
    }

    public LambdaCallCacheProperties getCache() {
        return cache;
    }

    public void setCache(LambdaCallCacheProperties cache) {
        this.cache = cache;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public LambdaCallError getError() {
        return error;
    }

    public void setError(LambdaCallError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
