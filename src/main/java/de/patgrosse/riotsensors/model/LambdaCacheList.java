package de.patgrosse.riotsensors.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Map;

/**
 * List of multiple lambdas and their cache contents
 *
 * @author Patrick Grosse &lt;patrick.grosse@uni-muenster.de&gt;
 */
public class LambdaCacheList {
    private Map<Integer, LambdaCacheEntry> lambdas;
    private int count;

    public Map<Integer, LambdaCacheEntry> getLambdas() {
        return lambdas;
    }

    public void setLambdas(Map<Integer, LambdaCacheEntry> lambdas) {
        this.lambdas = lambdas;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
