package de.patgrosse.riotsensors;

import de.patgrosse.riotsensors.constants.LambdaType;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RiotsensorsRESTServiceFactory.class)
public class RiotsensorsRESTServiceTest {
    @Autowired
    private RiotsensorsRESTServiceFactory riotsensorsRESTServiceFactory;

    @Autowired
    private RiotsensorsRESTServiceFactory riotsensorsRESTServiceFactory2;

    @Test
    public void testAutowire() {
        System.out.println("Checking if service is correctly autowired...");
        assertNotNull(riotsensorsRESTServiceFactory.createRiotsensorsRESTService("localhost", 9080, "v1"));
        assertSame(riotsensorsRESTServiceFactory, riotsensorsRESTServiceFactory2);
    }

    @Test
    @Ignore
    public void testLocal() {
        RiotsensorsRESTService service = riotsensorsRESTServiceFactory.createRiotsensorsRESTService("localhost", 9080, "v1");
        System.out.println(service.listLambdas(LambdaType.DOUBLE));
        System.out.println(service.listLambdasWithCache(LambdaType.INTEGER));
        System.out.println(service.callLambdaByName("testint", LambdaType.INTEGER));
        System.out.println(service.callLambdaById(0, LambdaType.INTEGER));
        // wrong type
        System.out.println(service.callLambdaById(0, LambdaType.DOUBLE));
        // not existing
        System.out.println(service.callLambdaById(5, LambdaType.STRING));
    }
}
